/* eslint-env mocha */

const assert = require('assert');
const { v4: uuid } = require('uuid');
const muhb = require('muhb');
const Nodecaf = require('nodecaf');
const redis = require('nodecaf-redis');

process.env.NODE_ENV = 'testing';

const init = require('../lib/main');

const LOCAL_HOST = 'http://127.0.0.1:8236';
const ACME_GATEWAY = process.env.ACME_GATEWAY || 'host.docker.internal';
const REDIS_HOST = process.env.REDIS_HOST || 'localhost';
const ACME_API_URL = process.env.ACME_API_URL || 'https://localhost:14000/dir';
const SERVICES = {
    unresponsive: { url: 'http://anything', endpoints: [ 'GET /foo' ] },
    backend: { url: 'http://localhost:8060', endpoints: [ 'GET /say/:something', 'GET /headers' ] },
    public: { url: 'http://localhost:8060', exposed: true },
    hostly: { domain: 'foobar', url: 'http://localhost:8060', exposed: true },
    httpsonly: { domain: ACME_GATEWAY, url: 'http://localhost:8060', secure: true, endpoints: [ 'GET /headers' ] }
};
const ACME_CONF = {
    api: ACME_API_URL,
    email: 'api-joe@gmail.com',
    challengePort: 5002
};

let app;

const authProvider = new Nodecaf({
    conf: { port: 8060 },
    autoParseBody: true,
    routes: [

        Nodecaf.get('/', ({ res }) => res.end()),

        Nodecaf.post('/auth', ({ res, body }) => {
            if(body)
                return res.status(400).end();
            res.end('ID: ' + uuid());
        }),

        Nodecaf.post('/replace', ({ res, body }) => {
            if(body)
                return res.status(400).end();
            res.end('ID: ' + uuid());
        }),

        Nodecaf.get('/get', ({ res }) => res.end()),

        Nodecaf.get('/say/:something', ({ params, res }) => res.text(params.something)),

        Nodecaf.get('/headers', ({ headers, res }) => res.json(headers)),

        Nodecaf.get('/public-only', ({ res }) => res.end('public-only'))
    ]
});

before(async function(){
    await authProvider.start();
});

after(async function(){
    await authProvider.stop();
});

beforeEach(async function(){
    this.timeout(4000);
    app = init();
    app.setup({
        port: 8236,
        redis: { host: REDIS_HOST },
        cookie: { name: 'foobaz', secret: 'FOOBAR' },
        services: SERVICES,
        diagnostics: true,
        auth: { url: 'http://localhost:8060/auth' },
        replace: { url: 'http://localhost:8060/replace' }
    });
    await app.start();
});

afterEach(async function(){
    await new Promise(done => setTimeout(done, 400));
    await app.stop();
});

describe('Startup', function(){

    it('Should boot just fine', async function(){
        const { status } = await muhb.get(LOCAL_HOST + '/');
        assert.strictEqual(status, 404);
    });

    it('Should boot just fine even without services', async function(){
        await app.restart({ services: null });
        const { status } = await muhb.get(LOCAL_HOST + '/');
        assert.strictEqual(status, 404);
    });

    it('Should fail when cannot connect to redis', async function(){
        this.timeout(3e3);
        await assert.rejects(app.restart({ redis: { port: 8676 } }));
    });

    it('Should fail if serivce config has unsupported keys', async function(){
        const p = app.restart({ services: { foo: { bar: 'baz' } } });
        await assert.rejects(p, /key 'bar'/ );
    });

});

describe('Discovery', function(){

    it('Should add endpoints advertised via redis channel', async function(){

        await app.global.redis.publish('joe:discovery', JSON.stringify({
            name: 'backend',
            endpoints: [ 'GET /get' ]
        }));

        await new Promise(done => setTimeout(done, 500));

        const { status } = await muhb.get(LOCAL_HOST + '/backend/get');
        assert.strictEqual(status, 200);
    });

    it('Should add services advertised via redis channel', async function(){

        await app.global.redis.publish('joe:discovery', JSON.stringify({
            name: 'new-serv',
            url: 'http://localhost:8060',
            endpoints: [ 'GET /get' ]
        }));

        await new Promise(done => setTimeout(done, 500));

        const { status } = await muhb.get(LOCAL_HOST + '/new-serv/get');
        assert.strictEqual(status, 200);
    });

    it('Should return all services currently installed', async function(){
        const { status, body } = await app.trigger('get', '/services');
        assert.strictEqual(status, 200);
        assert.strictEqual(body.httpsonly.name, 'httpsonly');
    });

});

describe('Proxy', function(){

    it('Should fail when service doesn\'t exist', async function(){
        const { status } = await muhb.get(LOCAL_HOST + '/nothing');
        assert.strictEqual(status, 404);
    });

    it('Should fail when service is unresponsive', async function(){
        this.timeout(4000);
        const { status } = await muhb.get(LOCAL_HOST + '/unresponsive/foo');
        assert.strictEqual(status, 503);
    });

    it('Should fail when endpoint doesn\'t exist', async function(){
        const { status } = await muhb.get(LOCAL_HOST + '/backend/public-only');
        assert.strictEqual(status, 404);
    });

    it('Should reach an exposed service endpoints', async function(){
        const { status, body } = await muhb.get(LOCAL_HOST + '/backend/headers');
        assert.strictEqual(status, 200);
        assert(body.includes('date'));
        const { status: s2, body: b2 } = await muhb.get(LOCAL_HOST + '/backend/say/foobar');
        assert.strictEqual(s2, 200);
        assert.strictEqual(b2, 'foobar');
    });

    it('Should reach any endpoint of exposed service [*service.exposed]', async function(){
        const { status, body } = await muhb.get(LOCAL_HOST + '/public/public-only');
        assert.strictEqual(status, 200);
        assert(body.includes('public-only'));
        const { status: s2 } = await muhb.get(LOCAL_HOST + '/public/nothing');
        assert.strictEqual(s2, 404);
    });

    it('Should preserve cookies from outside when setup [proxy.preserveCookies]', async function(){
        await app.restart({ proxy: { preserveCookies: true } });
        const { body } = await muhb.get(LOCAL_HOST + '/backend/headers', { cookies: { foo: 'bar' } });
        assert(body.includes('"cookie":"foo=bar'));
    });

    it('Should find service according to HOST header [*service.domain]', async function(){
        const { status, body } = await muhb.get(LOCAL_HOST + '/headers', { 'host': 'hostly' });
        assert.strictEqual(status, 200);
        assert(body.includes('{'));
    });
});

describe('Session', function(){

    describe('POST /login', function(){

        it('Should fail when can\'t reach auth server', async function(){
            this.timeout(2700);
            await app.restart({ auth: { url: 'http://nothing' } });
            const { status } = await muhb.post(LOCAL_HOST + '/login');
            assert.strictEqual(status, 500);
        });

        it('Should return authentication failure when not 200', async function(){
            const { status } = await muhb.post(LOCAL_HOST + '/login', { auto: true }, 'must fail');
            assert.strictEqual(status, 400);
        });

        it('Should return ok when auth succeeds', async function(){
            const { status } = await muhb.post(LOCAL_HOST + '/login');
            assert.strictEqual(status, 200);
        });

        it('Should include claim header when proxying for logged in client', async function(){
            const { status, cookies } = await muhb.post(LOCAL_HOST + '/login');
            assert.strictEqual(status, 200);
            const { status: s2, body } = await muhb.get(LOCAL_HOST + '/backend/headers', { cookies });
            assert.strictEqual(s2, 200);
            assert(body.includes('"x-claim":"ID'));
        });

    });

    describe('POST /mimic', function(){

        it('Should return authentication failure when not 200', async function(){
            const { cookies } = await muhb.post(LOCAL_HOST + '/login');
            const { status } = await muhb.post(LOCAL_HOST + '/mimic', { cookies, auto: true }, 'must fail');
            assert.strictEqual(status, 400);
        });

        it('Should return ok when auth succeeds', async function(){
            const { cookies } = await muhb.post(LOCAL_HOST + '/login');
            const { status } = await muhb.post(LOCAL_HOST + '/mimic', { cookies });
            assert.strictEqual(status, 200);
        });

        it('Should include claim header when proxying for logged in client', async function(){
            const { cookies } = await muhb.post(LOCAL_HOST + '/login');
            const { status } = await muhb.post(LOCAL_HOST + '/mimic', { cookies });
            assert.strictEqual(status, 200);
            const { status: s2, body } = await muhb.get(LOCAL_HOST + '/backend/headers', { cookies });
            assert.strictEqual(s2, 200);
            assert(body.includes('"x-claim":"ID'));
        });

    });

    describe('POST /logout', function(){

        it('Should force expire an active session', async function(){
            await app.restart({ session: { timeout: '1d' } });
            const { cookies } = await muhb.post(LOCAL_HOST + '/login');
            const { headers } = await muhb.post(LOCAL_HOST + '/logout', { cookies });
            assert(headers['set-cookie'][0].indexOf('01 Jan 1970') > 0);
        });

    });

});

describe('Events', function(){
    const WebSocket = require('ws');

    let backend, logWS, pubWS, logWS2, logClaim;

    before(async function(){
        backend = await redis({ host: REDIS_HOST });
    });

    beforeEach(async function(){
        const { cookies } = await muhb.post(LOCAL_HOST + '/login');
        const { cookies: c2 } = await muhb.post(LOCAL_HOST + '/login');
        logClaim = await backend.get('joe:sess:' + cookies.foobaz.substring(0, 36));
        await backend.sAdd('foobarg', logClaim);
        pubWS = new WebSocket('ws://localhost:8236/events');
        logWS = new WebSocket('ws://localhost:8236/events', {
            headers: { 'Cookie': 'foobaz=' + cookies.foobaz }
        });
        logWS2 = new WebSocket('ws://localhost:8236/events', {
            headers: { 'Cookie': 'foobaz=' + c2.foobaz }
        });
    });

    afterEach(function(){
        pubWS.close();
        logWS.close();
        logWS2.close();
    });

    after(function(){
        backend.del('foobarg');
        backend.close();
    });

    it('Should drop connections to paths other than /events', function(done){
        const ws = new WebSocket('ws://localhost:8236/foobar');
        ws.on('error', () => done());
    });

    it('Should accept Authenticated WS clients', function(done){
        logWS.on('open', () => {
            setTimeout(() => {
                app.global.wss.clients.forEach(ws => assert(ws.claim || ws.public));
                done();
            }, 400);
        });
    });

    it('Should kick clients when stopping', function(done){
        this.timeout(3e3);
        pubWS.on('open', () => setTimeout(() => app.stop(), 2e3));
        pubWS.on('close', () => done());
    });

    it('Should broadcast to logged in clients', function(done){
        let c = 0, r = 0;
        const cfn = () => {
            c == 1 &&
                backend.publish('joe:ws:event', '{"broadcast":true,"data":"foobar"}');
            c++;
        };
        const rfn = msg => {
            assert.strictEqual(msg.toString(), '"foobar"');
            r > 0 && done();
            r++;
        }
        logWS2.on('open', cfn);
        logWS.on('open', cfn);
        logWS2.on('message', rfn);
        logWS.on('message', rfn);
    });

    it('Should notify only public clients', function(done){
        let c = 0;
        const cfn = () => {
            c == 1 &&
                backend.publish('joe:ws:event', '{"public":true,"data":"foobar"}');
            c++;
        };
        pubWS.on('open', cfn);
        logWS.on('open', cfn);
        pubWS.on('message', () => done());
        logWS.on('message', () => done(new Error('Logged client received public message')));
    });

    it('Should notify only targeted clients', function(done){
        let c = 0;
        const cfn = () => {
            c == 2 &&
                backend.publish('joe:ws:event', '{"target":"' + logClaim + '","data":"foobar"}');
            c++;
        };

        pubWS.on('open', cfn);
        logWS.on('open', cfn);
        logWS2.on('open', cfn);
        logWS.on('message', () => setTimeout(done, 400));
        pubWS.on('message', () => done(new Error('Logged client received public message')));
        logWS2.on('message', () => done(new Error('Logged client received public message')));
    });

    it('Should notify only targeted group', function(done){
        let c = 0;
        const cfn = () => {
            c == 2 &&
                backend.publish('joe:ws:event', '{"members":"foobarg","data":"foobar"}');
            c++;
        };
        pubWS.on('open', cfn);
        logWS.on('open', cfn);
        logWS2.on('open', cfn);
        logWS.on('message', () => done());
        pubWS.on('message', () => done(new Error('Logged client received public message')));
        logWS2.on('message', () => done(new Error('Logged client received public message')));
    });

    it('Should notify only targeted group with exception', function(done){
        let c = 0;
        const cfn = () => {
            c == 2 &&
                backend.publish('joe:ws:event', `{"except":["${logClaim}"], "members":"foobarg","data":"foobar"}`);
            c++;
        };
        pubWS.on('open', cfn);
        logWS.on('open', cfn);
        logWS2.on('open', cfn);
        logWS.on('message', () => done(new Error('Logged client received public message')));
        pubWS.on('message', () => done(new Error('Logged client received public message')));
        logWS2.on('message', () => done(new Error('Logged client received public message')));
        setTimeout(done, 400);
    });

    it('Should notify only targeted clients except within group', function(done){
        let c = 0;
        const cfn = () => {
            c == 2 &&
                backend.publish('joe:ws:event', `{"target":["${logClaim}"], "notMembers":"foobarg","data":"foobar"}`);
            c++;
        };
        pubWS.on('open', cfn);
        logWS.on('open', cfn);
        logWS2.on('open', cfn);
        logWS.on('message', () => done(new Error('Logged client received public message')));
        pubWS.on('message', () => done(new Error('Logged client received public message')));
        logWS2.on('message', () => done(new Error('Logged client received public message')));
        setTimeout(done, 400);
    });

});

describe('ACME', function(){
    let redisClient;

    before(async function(){
        redisClient = await redis({ host: REDIS_HOST });
    });

    after(function(){
        redisClient.close();
    });

    beforeEach(async function(){
        await redisClient.del('joe:privateKey');
        await redisClient.del('joe:domains:' + ACME_GATEWAY);
        await app.restart({ acme: ACME_CONF });
    });

    const acme = require('acme-client');
    const https = require('https');
    const tls = require('tls');

    // https://github.com/letsencrypt/pebble#avoiding-client-https-errors
    // https://github.com/letsencrypt/pebble/blob/main/test/certs/pebble.minica.pem
    const pebbleCA = `-----BEGIN CERTIFICATE-----
MIIDCTCCAfGgAwIBAgIIJOLbes8sTr4wDQYJKoZIhvcNAQELBQAwIDEeMBwGA1UE
AxMVbWluaWNhIHJvb3QgY2EgMjRlMmRiMCAXDTE3MTIwNjE5NDIxMFoYDzIxMTcx
MjA2MTk0MjEwWjAgMR4wHAYDVQQDExVtaW5pY2Egcm9vdCBjYSAyNGUyZGIwggEi
MA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQC5WgZNoVJandj43kkLyU50vzCZ
alozvdRo3OFiKoDtmqKPNWRNO2hC9AUNxTDJco51Yc42u/WV3fPbbhSznTiOOVtn
Ajm6iq4I5nZYltGGZetGDOQWr78y2gWY+SG078MuOO2hyDIiKtVc3xiXYA+8Hluu
9F8KbqSS1h55yxZ9b87eKR+B0zu2ahzBCIHKmKWgc6N13l7aDxxY3D6uq8gtJRU0
toumyLbdzGcupVvjbjDP11nl07RESDWBLG1/g3ktJvqIa4BWgU2HMh4rND6y8OD3
Hy3H8MY6CElL+MOCbFJjWqhtOxeFyZZV9q3kYnk9CAuQJKMEGuN4GU6tzhW1AgMB
AAGjRTBDMA4GA1UdDwEB/wQEAwIChDAdBgNVHSUEFjAUBggrBgEFBQcDAQYIKwYB
BQUHAwIwEgYDVR0TAQH/BAgwBgEB/wIBADANBgkqhkiG9w0BAQsFAAOCAQEAF85v
d40HK1ouDAtWeO1PbnWfGEmC5Xa478s9ddOd9Clvp2McYzNlAFfM7kdcj6xeiNhF
WPIfaGAi/QdURSL/6C1KsVDqlFBlTs9zYfh2g0UXGvJtj1maeih7zxFLvet+fqll
xseM4P9EVJaQxwuK/F78YBt0tCNfivC6JNZMgxKF59h0FBpH70ytUSHXdz7FKwix
Mfn3qEb9BXSk0Q3prNV5sOV3vgjEtB4THfDxSz9z3+DepVnW3vbbqwEbkXdk3j82
2muVldgOUgTwK8eT+XdofVdntzU/kzygSAtAQwLJfn51fS1GvEcYGBc1bDryIqmF
p9BI7gVKtWSZYegicA==
-----END CERTIFICATE-----`;
    acme.axios.defaults.httpsAgent = new https.Agent({ ca: pebbleCA, keepAlive: false });

    function getCertFor(domain){
        return new Promise(done => {
            const socket = tls.connect({
                port: 8236,
                host: 'localhost',
                servername: domain,
                rejectUnauthorized: false
            }, () => {
                const cert = socket.getPeerX509Certificate();
                socket.destroy();
                done(cert);
            });
        });
    }

    it('Should handle SSL when ACME is setup', async function(){
        this.timeout(20e3);
        const cert = await getCertFor(ACME_GATEWAY);
        assert.strictEqual(cert.subject, 'CN=' + ACME_GATEWAY);
        assert(cert.issuer.indexOf('Pebble') > 0);
    });

    it('Should reuse SSl cert while still valid', async function(){
        this.timeout(20e3);
        const cert1 = await getCertFor(ACME_GATEWAY);
        const cert2 = await getCertFor(ACME_GATEWAY);
        assert.strictEqual(cert1.subject, 'CN=' + ACME_GATEWAY);
        assert.strictEqual(cert1.serialNumber, cert2.serialNumber);
    });

    it('Should default to Self Signed cert in case of bad domain name', async function(){
        this.timeout(20e3);
        const cert = await getCertFor('127.0.0.2');
        assert(cert.issuer.indexOf('API Joe') > 0);
    });

    it('Should redirect regular http calls to https', async function(){
        const { status, headers } = await muhb.get('http://127.0.0.1:5002/headers');
        assert.strictEqual(status, 301);
        assert.strictEqual(headers.location, 'https://127.0.0.1:5002/headers');
    });

});

describe('Settings', function(){

    it('Should create signed cookie when auth succeeds [cookie.name]', async function(){
        await app.restart({ cookie: { name: 'foobar' } });
        const { cookies } = await muhb.post(LOCAL_HOST + '/login');
        assert.strictEqual(cookies.foobar.charAt(36), '.');
    });

    it('Should add setup header when proxying after auth [cookie.*][proxy.claimHeader]', async function(){
        await app.restart({ proxy: { claimHeader: 'X-Foo' } });
        const { cookies } = await muhb.post(LOCAL_HOST + '/login');
        const { body } = await muhb.get(LOCAL_HOST + '/backend/headers', { cookies });
        assert(body.includes('"x-foo":'));
    });

    it('Should expire auth data after the setup timeout [session.timeout]', async function(){
        await app.restart({ session: { timeout: '1s' } });
        const { cookies } = await muhb.post(LOCAL_HOST + '/login');
        await new Promise(done => setTimeout(done, 1500));
        const { body } = await muhb.get(LOCAL_HOST + '/backend/headers', { cookies });
        assert(body.indexOf('x-claim') < 0);
    });

    it('Should POST to specified URL when auth succeeds [auth.onSuccess]', function(done){
        const serv = require('http').createServer((req, res) => {
            assert.strictEqual(req.headers['x-joe-auth'], 'success');
            serv.close();
            res.end();
            done();
        });
        serv.listen(2345);
        (async () => {
            await app.restart({ auth: { onSuccess: 'http://localhost:2345' } });
            await muhb.post(LOCAL_HOST + '/login');
        })();
    });

});

describe('Regression', function(){

    it('Should respond to path /', async function(){
        const { status } = await muhb.get(LOCAL_HOST + '/', { 'host': 'hostly' });
        assert.strictEqual(status, 200);
    });

    it('Should be ok being logged in successively (nodecaf fixed bug)', async function(){
        const { cookies } = await muhb.post(LOCAL_HOST + '/login');
        await muhb.post(LOCAL_HOST + '/logout', { cookies });
        const { cookies: cs } = await muhb.post(LOCAL_HOST + '/login');
        await muhb.post(LOCAL_HOST + '/logout', { cookies: cs });
        const { headers } = await muhb.post(LOCAL_HOST + '/login');
        assert(headers['set-cookie'][0].indexOf('Max-Age=0') < 0);
    });

});
