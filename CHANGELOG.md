# API Joe Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v0.4.4] - 2022-09-23

### Added
- log entry when new services are discovered
- forwarded client IP to all requests going to backend
- feature to replace session

### Fixed
- proxy to always pass back all backend response headers
- logout endpoint to not accept unauthenticated request

## [v0.4.3] - 2022-06-08

### Added
- ability to add new services from discovery feature
- session id passed as header `X-Joe-Sess-Id` to auth backend

### Fixed
- query string not being passed down the proxy

## [v0.4.2] - 2021-12-03

### Fixed
- bug accepting service config from redis

## [v0.4.1] - 2021-12-03

### Added
- automatic redis discovery feature to startup

## [v0.4.0] - 2021-11-30

### Added
- redis channel so services can push updates to their endpoints

### Changed
- WS redis keys to be indexed by session id instead of user claim
- redis sessions keys prepending a joe string to prevent third-party overlapping

## [v0.3.6] - 2021-11-30

### Added
- redis events on logout and login

### Fixed
- crash when trying to parse bad JSON from redis events channel
- client cookies being passed along to auth backend
- login request not proxying client headers to auth backend

### Changed
- login endpoint to relay auth backend response body back to client

## [v0.3.5] - 2021-03-12

### Fixed
- cookie conf security being disregarded

## [v0.3.4] - 2020-12-14

### Fixed
- cookies being automatically expired after a few logins

## [v0.3.3] - 2020-12-12

### Fixed
- path / being responded with 404

## [v0.3.2] - 2020-12-11

### Added
- config item for ACME challenge server port

## [v0.3.1] - 2020-12-11

### Added
- missing support for params on endpoint paths

## Fixed
- public ws subscriptions receiving broadcast messages
- error when `services` is not defined in the configuration

## [v0.3.0] - 2020-11-20

### Added
- 'exposed' keyword to open service to requests on any path
- multi-domain config for detecting service based on host header
- ACME feature
- Redis to WebSocket backend events feature

### Fixed
- Dockerfile building bug

### Removed
- websocket proxy support

## [v0.2.1] - 2020-04-17

### Changed
- deploy process slightly

## [v0.2.0] - 2020-03-31

### Added
- session uuid in order to not expose the actual claim data

### Fixed
- re-applying gzip compression when reading from services
- claim data not being proxied to websocket requests
- unwanted headers passed from services to client

## [v0.1.3] - 2020-02-21

### Added
- support for proxying WebSocket connections to services

## [v0.1.2] - 2020-02-14

### Changed
- nodecaf patch version (upgrade)
- app name attribute to API Joe

## [v0.1.1] - 2020-02-12

### Added
- setting for the auth request timeout
- optional command line argument for setting config file path
- setting for triggering a webhook on successful auth

### Fixed
- bug when using miliseconds as sesison expiration time (expects seconds)
- docker image error when no external config file was mounted
- docker image to use unprevilleged user by default
- cookie clearing when accessing expired session

### Changed
- logging strategy to new Nodecaf stdout logging
- default port to 9000 regardless of HTTPS

## [v0.1.0] - 2019-10-16
- First officially published version.

[v0.1.0]: https://gitlab.com/GCSBOSS/api-joe/-/tags/v0.1.0
[v0.1.1]: https://gitlab.com/GCSBOSS/api-joe/-/tags/v0.1.1
[v0.1.2]: https://gitlab.com/GCSBOSS/api-joe/-/tags/v0.1.2
[v0.1.3]: https://gitlab.com/GCSBOSS/api-joe/-/tags/v0.1.3
[v0.2.0]: https://gitlab.com/GCSBOSS/api-joe/-/tags/v0.2.0
[v0.3.0]: https://gitlab.com/GCSBOSS/api-joe/-/tags/v0.3.0
[v0.3.1]: https://gitlab.com/GCSBOSS/api-joe/-/tags/v0.3.1
[v0.3.2]: https://gitlab.com/GCSBOSS/api-joe/-/tags/v0.3.2
[v0.3.3]: https://gitlab.com/GCSBOSS/api-joe/-/tags/v0.3.3
[v0.3.4]: https://gitlab.com/GCSBOSS/api-joe/-/tags/v0.3.4
[v0.3.5]: https://gitlab.com/GCSBOSS/api-joe/-/tags/v0.3.5
[v0.3.6]: https://gitlab.com/GCSBOSS/api-joe/-/tags/v0.3.6
[v0.4.0]: https://gitlab.com/GCSBOSS/api-joe/-/tags/v0.4.0
[v0.4.1]: https://gitlab.com/GCSBOSS/api-joe/-/tags/v0.4.1
[v0.4.2]: https://gitlab.com/GCSBOSS/api-joe/-/tags/v0.4.2
[v0.4.3]: https://gitlab.com/GCSBOSS/api-joe/-/tags/v0.4.3
