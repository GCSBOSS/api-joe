const redis = require('nodecaf-redis');
const acme = require('acme-client');
const Nodecaf = require('nodecaf');
const periodo = require('periodo');
const https = require('https');
const http = require('http');

const { onServiceUpdate, installServices } = require('./services');
const { onBackendEvent, buildWSServer } = require('./events');
const routes = require('./routes');
const { SNICallback, replyChallenge } = require('./acme');

module.exports = () => new Nodecaf({
    routes,
    conf: __dirname + '/default.toml',

    server(app){
        const server = app.conf.acme
            ? https.createServer({ SNICallback: SNICallback.bind(app) })
            : http.createServer();

        server.on('upgrade', (req, socket, head) =>
            app.global.wss.handleUpgrade(req, socket, head, (ws, req) =>
                app.global.wss.emit('connection', ws, req)));

        return server;
    },

    async startup({ conf, global, call, log }){
        const envIsProd = process.env.NODE_ENV == 'production';

        global.cookieOpts = {
            maxAge: periodo(conf.session.timeout).time,
            secure: conf.cookie.secure == null ? envIsProd : conf.cookie.secure,
            httpOnly: true,
            sameSite: conf.cookie.sameSite,
            signed: true
        };

        global.redisTimeout = periodo(conf.session.timeout, 's').time / 1000;

        global.redis = await redis(conf.redis, true);

        global.redis.sub.subscribe('joe:ws:event', function(msg){
            try{
                msg = JSON.parse(msg);
            }
            catch(err){
                return log.warn({ err, data: msg });
            }

            call(onBackendEvent, msg);
        });

        global.redis.sub.subscribe('joe:discovery', msg => call(onServiceUpdate, msg));

        if(conf.acme){
            let accountKey = await global.redis.get('joe:privateKey');
            if(!accountKey){
                accountKey = await acme.forge.createPrivateKey();
                global.redis.set('joe:privateKey', accountKey);
            }

            global.contexts = {};
            const directoryUrl = conf.acme.api || /* istanbul ignore next */(envIsProd
                ? acme.directory.letsencrypt.production
                : acme.directory.letsencrypt.staging);
            global.acmeClient = new acme.Client({ directoryUrl, accountKey });

            global.chlngServer = http.createServer(replyChallenge.bind(global));
            global.chlngServer.listen(conf.acme.challengePort ?? 80);
        }

        global.wss = buildWSServer(this);

        global.services = {};
        global.domains = {};

        await call(installServices);
    },

    async shutdown({ global }){
        await global.wss.doClose();
        global.chlngServer && global.chlngServer.close();
        await global.redis.close();
    }
});
