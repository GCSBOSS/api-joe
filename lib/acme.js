const forge = require('node-forge');
const acme = require('acme-client');
const tls = require('tls');

function getSelfSignedCert(domain){

    const { privateKey, publicKey } = forge.pki.rsa.generateKeyPair(2048);
    const cert = forge.pki.createCertificate();

    cert.publicKey = publicKey;
    cert.serialNumber = '01';
    cert.validity.notBefore = new Date();
    cert.validity.notAfter = new Date();
    cert.validity.notAfter.setFullYear(new Date().getFullYear() + 1);

    cert.setSubject([ { name: 'commonName', value: domain } ]);
    cert.setIssuer([ { name: 'commonName', value: 'API Joe Self-Signed Certificate' } ]);
    cert.sign(privateKey);

    const expirey = cert.validity.notAfter;
    expirey.setDate(expirey.getDate() - 3);

    return {
        key: forge.pki.privateKeyToPem(privateKey),
        cert: forge.pki.certificateToPem(cert),
        expirey
    };
}

function createContext({ cert, key, expirey }){
    const ctx = tls.createSecureContext({ key, cert });
    ctx.expireAt = expirey;
    return ctx;
}

function isContextStillValid(pool, domain){
    return domain in pool && pool[domain].expireAt > new Date();
}

function challengeCreate(authz, { /*type,*/ token }, keyAuthorization) {
    //if(type === 'http-01'){
    this.global.redis.set('joe:challenges:' + token, keyAuthorization);
    this.log.debug({ type: 'acme' }, 'Created challenge %s', token);
}

async function challengeRemove(authz, { /*type,*/ token }) {
    //if(type === 'http-01'){
    const r = await this.global.redis.del('joe:challenges:' + token);
    r && this.log.debug({ type: 'acme' }, 'Removed challenge %s', token);
}

module.exports = {

    async replyChallenge(req, res){

        if(req.url.indexOf('/.well-known/acme-challenge/') == 0){
            const token = req.url.substring(28);
            const key = await this.redis.get('joe:challenges:' + token);

            if(key)
                return res.end(key);

            res.statusCode = 404;
            return res.end();
        }

        res.writeHead(301, { Location: `https://${req.headers.host}${req.url}` });
        res.end();
    },

    async SNICallback(domain, cb){
        // this => app

        // TODO IS DOMAIN A SERVICE ?

        // TODO IS this needed due to when SNICallback is called?
        if(isContextStillValid(this.global.contexts, domain))
            return cb(null, this.global.contexts[domain]);

        var data = await this.global.redis.hGetAll('joe:domains:' + domain);

        if(!data?.key){
            const [ key, csr ] = await acme.crypto.createCsr({
                commonName: domain
            });

            data = { key };

            try{
                data.cert = await this.global.acmeClient.auto({
                    csr, email: this.conf.acme.email,
                    termsOfServiceAgreed: true,
                    skipChallengeVerification: true,
                    challengeCreateFn: challengeCreate.bind(this),
                    challengeRemoveFn: challengeRemove.bind(this)
                });

                this.log.info({ type: 'acme' }, 'Generated new cert for \'%s\'', domain);

                const info = acme.crypto.readCertificateInfo(data.cert);
                data.expirey = info.notAfter;
                data.expirey.setDate(data.expirey.getDate() - 3);

                this.global.redis.hSet('joe:domains:' + domain, 'cert', data.cert);
                this.global.redis.hSet('joe:domains:' + domain, 'key', data.key);
                this.global.redis.hSet('joe:domains:' + domain, 'expirey', data.expirey);
                this.global.redis.expireAt('joe:domains:' + domain, data.expirey.getTime());
            }
            catch(err){
                data = getSelfSignedCert(domain);
                this.log.warn({ err, type: 'acme', domain });
            }
        }
        const ctx = createContext(data);
        this.global.contexts[domain] = ctx;
        cb(null, ctx);
    }


};
