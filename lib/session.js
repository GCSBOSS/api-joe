const { request, post } = require('muhb');
const { v4: uuid } = require('uuid');

module.exports = {

    async match({ redisTimeout, signedCookies, cookieOpts, redis, conf, res, keep }){

        const sessid = signedCookies[conf.cookie.name];
        if(typeof sessid !== 'string')
            return;

        const claim = await redis.get('joe:sess:' + sessid);
        if(!claim){
            res.clearCookie(conf.cookie.name, cookieOpts);
            return;
        }

        keep('claim', claim);
        keep('sessid', sessid);
        redis.expire('joe:sess:' + sessid, redisTimeout);
    },

    async create({ redisTimeout, cookieOpts, conf, res, redis, headers, body }){
        body = await body.raw();

        const sessid = uuid();

        const authRes = await request({
            timeout: conf.auth.timeout,
            body,
            method: conf.auth.method,
            url: conf.auth.url,
            headers: { ...headers, ...conf.auth.headers, 'X-Joe-Sess-Id': sessid }
        });

        res.badRequest(authRes.status !== 200, authRes.body);
        res.cookie(conf.cookie.name, sessid, cookieOpts).end(authRes.body);

        redis.set('joe:sess:' + sessid, authRes.body, { EX: redisTimeout });
        redis.publish('joe:login', authRes.body);

        if(typeof conf.auth.onSuccess == 'string')
            post(conf.auth.onSuccess, { 'X-Joe-Auth': 'success' }, authRes.body);
    },

    destroy({ cookieOpts, claim, sessid, res, redis, conf }){
        res.unauthorized(!claim);
        res.clearCookie(conf.cookie.name, cookieOpts);
        res.end();
        redis.del('joe:sess:' + sessid)
        redis.publish('joe:logout', claim);
    },

    async replace({ body, conf, headers, sessid, res, cookieOpts, redis, redisTimeout }){
        res.unauthorized(!sessid, 'no session');

        body = await body.raw();

        const r = await request({
            timeout: conf.replace.timeout,
            body,
            method: conf.replace.method,
            url: conf.replace.url,
            headers: { ...headers, ...conf.replace.headers, 'X-Joe-Sess-Id': sessid }
        });

        res.badRequest(r.status !== 200, r.body);
        res.cookie(conf.cookie.name, sessid, cookieOpts).end(r.body);

        redis.set('joe:sess:' + sessid, r.body, { EX: redisTimeout });
    }

}
