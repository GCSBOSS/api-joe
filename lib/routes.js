const { post, all, get } = require('nodecaf');
const Session = require('./session');
const Proxy = require('./proxy');
const { routeRequest } = require('./services');

module.exports = [

    post('/login', Session.create),

    post('/logout', async function({ call }){
        await call(Session.match);
        call(Session.destroy);
    }),

    post('/mimic', async function({ call }){
        await call(Session.match);
        call(Session.replace);
    }),

    get('/services', function({ conf, res, services }){
        res.notFound(!conf.diagnostics);
        res.json(services);
    }),

    all(async function({ call }){
        call(routeRequest);
        await call(Session.match);
        call(Proxy.pass);
    })
]
