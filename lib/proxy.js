const http = require('http');

module.exports = {

    pass({ path, res, log, query, routing, conf, body, headers, sessid, claim, method, ip }){

        let target = routing.service.url + (routing.removePrefix
            ? path.substr(routing.service.name.length + 1)
            : path);

        const qs = new URLSearchParams(query).toString();
        if(qs)
            target += '?' + qs;

        const newHeaders = { ...headers };
        delete newHeaders[conf.proxy.claimHeader];
        delete newHeaders['accept-encoding'];

        newHeaders['x-joe-client-ip'] = ip;

        if(!conf.proxy.preserveCookies)
            delete newHeaders.cookie;

        if(sessid)
            newHeaders['joe-sessid'] = sessid;

        if(claim)
            newHeaders[conf.proxy.claimHeader] = encodeURIComponent(claim);

        // TODO timeout

        try{
            const newReq = http.request(target, { method, headers: newHeaders });
            body.pipe(newReq);
            newReq.on('response', beRes => {

                res.status(beRes.statusCode);

                let k;
                for(const s of beRes.rawHeaders)
                    if(k){
                        res.set(k, s);
                        k = undefined;
                    }
                    else
                        k = s;

                beRes.pipe(res);
            });

            newReq.on('error', err => {
                res.status(503).end();
                log.error({ err, target, class: 'proxy' });
            });
        }
        catch(err){
            res.status(503).end();
            log.error({ err, target, class: 'proxy' });
        }
    }

}
