const cookieSignature = require('cookie-signature');
const cookie = require('cookie');
const WebSocket = require('ws');
const url = require('url');

/* istanbul ignore next */
function checkClientsHealth(){
    // this => wss
    this.clients.forEach(ws => {
        if(ws.isAlive === false)
            return ws.terminate();
        ws.isAlive = false;
        ws.ping(Function.prototype);
    });
}

async function onConnect(ws, req){
    const addr = req.connection.remoteAddress;
    req.pathname = url.parse(req.url, true).pathname

    ws.isAlive = true;
    ws.on('pong', /* istanbul ignore next */ () => ws.isAlive = true);

    this.log.debug({ type: 'ws', addr }, 'New connection from %s', addr);

    const cookies = cookie.parse(req.headers.cookie || '');
    const signed = cookies[this.conf.cookie.name] || '';
    const sessid = cookieSignature.unsign(signed, this.conf.cookie.secret);

    ws.on('close', () => {
        this.log.debug({ type: 'ws', addr }, 'Closed connection from %s', addr);

        if(ws.claim){
            this.global.redis.del('joe:ws:' + sessid);
            this.global.redis.publish('joe:ws:close', ws.claim);
            clearInterval(ws.ei);
        }
    });

    /* istanbul ignore next */
    ws.on('error', err => this.log.error({ type: 'ws', addr, err }, 'Error in connection from %s', addr));

    const claim = sessid && await this.global.redis.get('joe:sess:' + sessid);

    if(claim){
        ws.claim = claim;
        this.global.redis.publish('joe:ws:connect', claim);
        this.global.redis.set('joe:ws:' + sessid, claim);
        ws.ei = setInterval(/* istanbul ignore next */ () =>
            this.global.redis.expire('joe:ws:' + claim, 12), 10e3);
    }
    else
        ws.public = true;
}

module.exports = {

    buildWSServer(app){
        const wss = new WebSocket.Server({ noServer: true, path: '/events' });
        wss.on('connection', onConnect.bind(app));
        wss.healthChecker = setInterval(checkClientsHealth.bind(wss), 30000);
        wss.doClose = function(){
            clearInterval(wss.healthChecker);
            const cps = [];
            wss.clients.forEach(ws => {
                cps.push(new Promise(done => ws.on('close', done)));
                ws.terminate();
            });
            return Promise.all(cps);
        };

        return wss;
    },

    async onBackendEvent({ redis, wss }, event){

        const { public: pub, target, except, members, notMembers, data, broadcast } = event;
        const targets = {};

        [].concat(target).forEach(c => targets[c] = true);
        if(members){
            const cs = await redis.sMembers(members);
            cs && cs.forEach(c => targets[c] = true);
        }

        [].concat(except).forEach(c => targets[c] = false);
        if(notMembers){
            const cs = await redis.sMembers(notMembers);
            cs && cs.forEach(c => targets[c] = false);
        }

        delete targets[undefined];

        const json = JSON.stringify(data);

        wss.clients.forEach(ws =>
            (ws.public ? pub : broadcast || targets[ws.claim]) && ws.send(json));
    }

}
