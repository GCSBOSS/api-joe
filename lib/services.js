const { pathToRegexp } = require('path-to-regexp');

const SERVICE_KEYS = { url: 1, endpoints: 1, domain: 1, secure: 1, exposed: 1, name: 1 };
const PRG_OPTS = { sensitive: true, strict: true };

function buildService(spec){

    const service = {};

    if(!spec || typeof spec !== 'object')
        return;

    if(!spec.name)
        spec.name = 'service' + Date.now();

    for(const key in spec)
        if(!(key in SERVICE_KEYS))
            throw new Error(`Unsupported key '${key}' on service '${spec.name}'`);

    Object.assign(service, spec);

    service.dynamicEndpoints = [];
    service.endpoints = {};

    if(Array.isArray(spec.endpoints))
        for(const endpoint of spec.endpoints)
            if(endpoint.indexOf(':') >= 0){
                const [ method, path ] = endpoint.split(' ');
                const regexp = pathToRegexp(path, [], PRG_OPTS);
                service.dynamicEndpoints.push({ method, regexp });
            }
            else
                service.endpoints[endpoint] = 1;

    return service;
}

module.exports = {

    onServiceUpdate({ services, log }, json){
        try{
            const serv = buildService(JSON.parse(json));
            services[serv.name] = { ...services[serv.name] ?? {}, ...serv };
            if(serv.domain)
                services[serv.domain] = services[serv.name];
            log.debug({ type: 'discovery', service: serv }, 'Discovered service \'%s\'', serv.name);
        }
        catch(err){
            log.warn({ err });
        }
    },

    async installServices({ services, conf, redis }){

        const cSpecs = conf.services ?? {};
        const dSpecs = await redis.hGetAll('joe:discovery');
        const names = [ ...new Set([ ...Object.keys(dSpecs), ...Object.keys(cSpecs) ]) ]

        for(const name of names){

            const spec = {
                ...cSpecs[name] ?? {},
                ...JSON.parse(dSpecs[name] ?? '{}'),
                name
            };

            const service = buildService(spec);

            services[name] = service;

            if(service.domain)
                services[service.domain] = service;
        }
    },

    routeRequest({ keep, path, headers, services, res, method }){
        const host = headers.host?.split(':')[0].toLowerCase();
        const [ , prefix, ...pa ] = path.split('/');
        const routing = {
            service: services[host] ?? services[prefix],
            removePrefix: !services[host],
        };
        keep('routing', routing);

        res.notFound(!routing.service);

        if(!routing.service.exposed){
            const p = routing.removePrefix ? pa : [ prefix, ...pa ];

            if(method + ' /' + p.join('/') in routing.service.endpoints)
                return;

            for(const ep of routing.service.dynamicEndpoints)
                if(ep.regexp.exec('/' + p.join('/')))
                    return;

            res.notFound(true);
        }
    }
}
